import Vue from 'vue'
import Vuex from 'vuex'
import { getItem, setItem, removeItem } from '@/utils/storage'

Vue.use(Vuex)

const USER_KEY = 'toutiao-user'
export default new Vuex.Store({
  state: {
    user: getItem(USER_KEY)
  },
  mutations: {
    setUser (state, user) {
      state.user = user

      // 为了防止刷新页面，造成数据丢失，
      // 需要将 token信息保存到 本地存储中
      setItem(USER_KEY, user)
    },
    removeUser(state) {
      state.user = null
      removeItem(USER_KEY)
    }
  },
  actions: {
  },
  modules: {
  }
})
