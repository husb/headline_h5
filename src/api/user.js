import request from '@/utils/request'
// 在 非组件模块中，获取store 必须使用 import 导入 store
// store模块导出了 store的实例 export default new Vuex.Store({...})
// import store from '@/store/'

/**
 * 登录注册
 * @param {user} data
 */
export const login = data => {
    return request({
        method: 'POST',
        url: '/app/v1_0/authorizations',
        data
    })
}

/**
 * 发送验证码
 */
export const sendSms = mobile => {
    return request({
        method: 'GET',
        url: `/app/v1_0/sms/codes/${mobile}`
    })
}

export const getCurrentUser = token => {
    return request({
        method: 'GET',
        url: '/app/v1_0/user'
        // 将添加请求头代码，转移到 axios的拦截器中
        // headers: {
        //     Authorization: `Bearer ${store.state.user.token}`
        // }
    })
}
