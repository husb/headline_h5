import Vue from 'vue'
import VueRouter from 'vue-router'
// import Home from '../views/Home.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/login',
    name: 'login',
    component: () => import('@/views/login')
  },
  {
    path: '/',
    alias: ['/index', '/home', '/home/index'],
    // name: 'layout',
    component: () => import('@/views/layout'),
    children: [
      {
        path: '',
        name: 'home',
        component: () => import('@/views/home/')
      },
      {
        path: '/feature',
        name: 'feature',
        component: () => import('@/views/feature/')
      },
      {
        path: '/catalog',
        name: 'catalog',
        component: () => import('@/views/catalog/')
      },
      {
        path: '/cart',
        name: 'cart',
        component: () => import('@/views/cart/')
      },
      {
        path: '/profile',
        name: 'profile',
        component: () => import('@/views/profile/')
      }
    ]
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
